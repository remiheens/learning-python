"""
Challenge Day 4 : A localized digital clock (time, date, day of week)
"""
import datetime
import locale
import time
import pytz

# sudo dpkg-reconfigure locales
locale.setlocale(locale.LC_TIME, 'fr_FR')

while True:
    # setup timezone
    x = datetime.datetime.now(tz=pytz.timezone('Europe/Paris'))
    # add end parameters to clear line and output in same line
    print(x.strftime("%H:%M:%S %A %d/%m/%Y"), end='\r')
    # sleep for a second
    time.sleep(1)
