"""
Challenge Day 9: Random quote generator
"""

import time
from random_word import RandomWords
from quote import quote

if input("Use random word ? [y/n] ") == "y":
    r = RandomWords()
    search = r.get_random_word()
else:
    search = input("Enter a word to search related quote : ")

number_of_quote = int(input("How many quote do you want ? "))

print("Looking for quote with term : ", search)

while True:
    rslt = quote(search, limit=number_of_quote)
    if rslt is not None:
        break
    print('Loading ...')
    time.sleep(1)

for itr in enumerate(rslt):
    print("> ", rslt[itr]['quote'])
