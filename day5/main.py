"""
Challenge Day 5 : Measurement calculator (weight, distance, area, volume, etc.)
"""


def weight_converter(value, from_unit, to_unit):
    """Define a function to convert weights"""
    return generic_converter({
        "kg": 1,
        "g": 1000,
        "lb": 2.20462,
        "oz": 35.274
    }, value, from_unit, to_unit)


def distance_converter(value, from_unit, to_unit):
    """Define a function to convert distances"""
    return generic_converter({
        "m": 1,
        "km": 0.001,
        "mi": 0.000621371,
        "ft": 3.28084,
        "yd": 1.09361
    }, value, from_unit, to_unit)


def area_converter(value, from_unit, to_unit):
    """Define a function to convert areas"""
    return generic_converter({
        "sq m": 1,
        "sq km": 0.000001,
        "sq mi": 0.000000386102,
        "sq ft": 10.7639,
        "sq yd": 1.19599
    }, value, from_unit, to_unit)


def volume_converter(value, from_unit, to_unit):
    """Define a function to convert volumes"""
    return generic_converter({
        "L": 1,
        "ml": 1000,
        "gal": 0.264172,
        "qt": 1.05669,
        "pt": 2.11338,
        "fl oz": 33.814
    }, value, from_unit, to_unit)


def generic_converter(coef, value, from_unit, to_unit):
    """Define a function to calculate a cross product"""
    return round(value * coef[to_unit] / coef[from_unit], 4)


def convert():
    """
    Define a function to get user input
    and call the appropriate converter function
    """
    print("What do you want to convert?")
    print("1. Weight")
    print("2. Distance")
    print("3. Area")
    print("4. Volume")
    choice = int(input("Enter your choice (1-4): "))
    if choice == 1:
        value = float(input("Enter the weight value: "))
        from_unit = input("Enter the weight unit (kg/g/lb/oz): ")
        to_unit = input("Enter the weight unit to convert to (kg/g/lb/oz): ")
        result = weight_converter(value, from_unit, to_unit)
        print(value, from_unit, "=", result, to_unit)
    elif choice == 2:
        value = float(input("Enter the distance value: "))
        from_unit = input("Enter the distance unit (m/mi/ft/yd): ")
        to_unit = input("Enter the distance unit to convert to (m/mi/ft/yd): ")
        result = distance_converter(value, from_unit, to_unit)
        print(value, from_unit, "=", result, to_unit)
    elif choice == 3:
        value = float(input("Enter the area value: "))
        from_unit = input("Enter the area unit (sq m/sq mi/sq ft/sq yd): ")
        to_unit = input("Enter the area unit to convert to (sq m/sq mi/sq ft/sq yd): ")
        result = area_converter(value, from_unit, to_unit)
        print(value, from_unit, "=", result, to_unit)
    elif choice == 4:
        value = float(input("Enter the volume value: "))
        from_unit = input("Enter the volume unit (L/ml/gal/qt/pt/fl oz): ")
        to_unit = input("Enter the volume unit to convert to (L/ml/gal/qt/pt/fl oz): ")
        result = volume_converter(value, from_unit, to_unit)
        print(value, from_unit, "=", result, to_unit)


convert()
