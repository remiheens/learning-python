"""
Challenge Day 2 : A letter, word, and sentence counter
"""

import os
import re

dir_path = os.path.dirname(os.path.realpath(__file__))

with open(dir_path+'/lipsum.txt', 'rt', encoding="utf-8") as file:
    text = file.read()
    file.close()

print("Text length :", len(text))
print("Letters count :", len(re.findall("\\w", text)))
print("Words count :", len(text.split()))
print("Sentences count :", len(re.split('[.!?]', text)))
print("Paragraphs count :", len(list(filter(None, re.split('\n', text)))))
