"""
Day 10: A stopwatch (for specific seconds, minutes, etc.)
"""
import time
from datetime import timedelta


class Lap:
    """Representation of a Lap"""

    def __init__(self) -> None:
        self.start_lap_time = .0
        self.stop_lap_time = .0

    def start(self):
        """start lap timer"""
        self.start_lap_time = time.time()

    def stop(self):
        """"stop lap timer"""
        self.stop_lap_time = time.time()

    def get_lap_time(self):
        """calculation of lap time"""
        return self.stop_lap_time - self.start_lap_time


class Stopwatch:
    """Representation of a stopwatch"""

    def __init__(self) -> None:
        self.starttime = .0
        self.laps = []
        self.current_lap = None
        self.total_time = .0

    def started(self) -> bool:
        """boolean method to determine if stopwatch started"""
        if self.starttime == .0:
            return False
        return True

    def start(self):
        """start the stop watch"""
        if not self.started():
            self.starttime = time.time()
        self.current_lap = Lap()
        self.current_lap.start()

    def stop(self):
        """stop stopwatch and save lap"""
        self.current_lap.stop()
        self.laps.append(self.current_lap)

    def lap(self):
        """create a lap and append to history"""
        self.stop()
        self.total_time += self.current_lap.get_lap_time()
        self.start()

    def print_last_lap(self):
        """Output last lap stats"""
        print("Last lap time = ",
              time_to_human(self.laps[-1].get_lap_time()),
              "total time = ",
              time_to_human(self.total_time))

    def print_laps(self):
        """Output list of laps"""
        total_time = .0
        for lap in self.laps:
            print("Lap time = ", time_to_human(lap.get_lap_time()), "total time = ",  time_to_human(total_time))
            total_time += lap.get_lap_time()


def time_to_human(seconds):
    """Method to convert seconds in float to a human-readable string"""
    return str(timedelta(seconds=seconds))


def main():
    """entrypoint"""
    stopwatch = Stopwatch()

    value = ""
    print("Press ENTER for each lap.\nType Q and press ENTER to stop.")
    stopwatch.start()
    while True:
        value = input()
        if value.lower() == "q":
            stopwatch.stop()
            break

        stopwatch.lap()
        stopwatch.print_last_lap()

    stopwatch.print_laps()
    del stopwatch


if __name__ == '__main__':
    main()
