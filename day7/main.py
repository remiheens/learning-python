"""
Challenge Day 7:  a basic calculator
"""

import re


def add(number_1, number_2):
    """add function"""
    return number_1 + number_2


def substract(number_1, number_2):
    """substract function"""
    return number_1 - number_2


def multiply(number_1, number_2):
    """multiply function"""
    return number_1 * number_2


def divide(number_1, number_2):
    """divide function"""
    return number_1 / number_2


def main():
    """Entrypoint"""
    while True:
        syntax = input("Enter some basic calcul : ")
        matches = re.search(r'([0-9]*)([\*\/\+\-])([0-9]*)', syntax)
        number_1 = float(matches.group(1))
        number_2 = float(matches.group(3))
        if matches.group(2) == "+":
            result = add(number_1, number_2)
        elif matches.group(2) == "-":
            result = substract(number_1, number_2)
        elif matches.group(2) == "*":
            result = multiply(number_1, number_2)
        elif matches.group(2) == "/":
            result = divide(number_1, number_2)

        print(matches.group(1), matches.group(2), matches.group(3), "=", result)


if __name__ == '__main__':
    main()
