"""
Challenge Day 1 : A number converter (binary, octal, hexadecimal, custom bas,)
"""
# ask for a number
number = int(input("Enter a int : "))
print("Number selected : " + str(number))

# Convert into binary
binary = bin(number)
print(binary)

binary = f'{number:b}'
print(binary)

# Convert in Hexa
hexa = hex(number)
print(hexa)

hexa = f'{number:x}'
print(hexa)

# Convert in octal
octal = oct(number)
print(octal)

octal = f'{number:o}'
print(octal)
