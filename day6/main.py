"""
Challenge Day 6 : A tic-tac-toe Game
"""
import numpy as np


class TicTacToe:
    """Class definition for a tic tac toe game """
    def __init__(self):
        self.board = [' '] * 9
        self.player = "X"

    def print_board(self):
        """Print game board in ascii mode"""
        print(self.board[0], '|', self.board[1], '|', self.board[2])
        print('--|---|--')
        print(self.board[3], '|', self.board[4], '|', self.board[5])
        print('--|---|--')
        print(self.board[6], '|', self.board[7], '|', self.board[8])

    def check_win(self):
        """Check if win combination"""
        combinations = [
            [0, 1, 2], [3, 4, 5], [6, 7, 8],  # lines
            [0, 3, 6], [1, 4, 7], [2, 5, 8],  # columns
            [0, 4, 8], [2, 4, 6]  # diagonals
        ]
        # check each combinations
        for pattern in combinations:
            # if each cell equals others and not empty, it's a win !
            if self.board[pattern[0]] == self.board[pattern[1]] == self.board[pattern[2]] != " ":
                return True
        return False

    def transform_address_to_cell(self, matrix):
        """Transform x,y into array index"""
        i = matrix.split(',')
        i[0] = int(i[0])
        i[1] = int(i[1])
        cell_index = i[0] + i[1]
        if 0 > i[0] or i[0] > 3:
            raise ValueError
        if 0 > i[1] or i[1] > 3:
            raise ValueError

        if i[1] == 1:
            cell_index -= 2
        elif i[1] == 3:
            cell_index += 2
        return cell_index

    def bot_play(self):
        """Simulate player for forever alone player"""
        empty_cells = np.where(np.array(self.board) == " ")[0]
        cell = np.random.choice(empty_cells)
        self.board[cell] = "O"

    def play(self, two_player):
        """Let's play"""
        while True:
            if " " not in self.board:
                print("It's a tie!")
                break
            if self.check_win():
                print("It's a win !")
                break
            move = input("Enter cell address (col,line): ")
            try:
                cell = self.transform_address_to_cell(move)
                if self.board[cell] != " ":
                    print("Invalid cell. Already taken.")
                    continue
                self.board[cell] = self.player
                if two_player:
                    self.player = "O" if self.player == "X" else "X"
                else:
                    self.bot_play()
                self.print_board()
            except ValueError:
                print("Invalid cell address")


game = TicTacToe()
game.play(input("One player ? [y]") != "y")
