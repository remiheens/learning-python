"""
Challenge Day 8: A world (digital) clock for at least 5 cities on different continents
"""

import json
import datetime
import locale
import os
import time
import pytz
from dotenv import load_dotenv
import requests


def main():
    """entry point"""

    load_dotenv()

    # sudo dpkg-reconfigure locales
    locale.setlocale(locale.LC_TIME, 'fr_FR')
    cities = ["Bruxelles", "Tokyo", "Los Angeles", "Nouméa", "Le Caire", "Reykjavik"]
    tzs = []

    # Getting timezone location for cities
    for city in cities:
        response = requests.get("https://timezone.abstractapi.com/v1/current_time/?api_key="
                                + os.getenv("ABSTRACT_API_KEY")
                                + "&location="+city, timeout=5)
        time.sleep(1)
        json_response = json.loads(response.content)
        tzs.append(json_response['timezone_location'])

    while True:
        line = ""
        for timezone in tzs:
            # setup timezone
            datetime_timezoned = datetime.datetime.now(tz=pytz.timezone(timezone))
            line = line + timezone + ":" + datetime_timezoned.strftime("%H:%M:%S %d/%m/%Y") + "\t"

        # add end parameters to clear line and output in same line
        print(line, end='\r')
        # sleep for a second
        time.sleep(1)


if __name__ == '__main__':
    main()
