"""
Challenge Day 3 : The currency converter
"""
import json
import os
from dotenv import load_dotenv
import requests

load_dotenv()

euro = input("Enter value (€) :")
print("Try ot convert", euro, '€')

response = requests.get(
    'https://exchange-rates.abstractapi.com/v1/live/?api_key='
    + os.getenv("ABSTRACT_API_KEY")
    + '&base=EUR',
    timeout=3)
json_response = json.loads(response.content)

for currency in json_response['exchange_rates']:
    rate = json_response['exchange_rates'][currency]
    print(currency, int(euro)*float(rate))
